/**
 * Load pixelated texture.
 *
 * path(String) path to file
 * isAtlas(bool) true if texture is an atlas (defaults to false)
 * rows(int) number of rows in the texture (required if isAtlas=true)
 * cols(int) number of columns in the texture (required if isAtlas=true)
 */
THREE.TextureLoader.prototype.loadPixelArt = function(path, isAtlas, rows, cols) {
	var tex = this.load(path);
	tex.magFilter = THREE.NearestFilter; // Pixelated

	isAtlas = typeof isAtlas !== 'undefined' ?  isAtlas : false;
	if (isAtlas) {
		tex.matrixAutoUpdate = false; // Will manually set the UV transform matrix
		tex.matrix.elements[0] = 1./rows;
		tex.matrix.elements[4] = 1./cols;
	}
	return tex;
};

/**
 * Load pixelated texture from a subregion (in the top left) of a square image whose side is a power of two.
 *
 * path(String) path to file
 * pow(int) "power of two", size of the texture's side
 * width(int) width in pixel of the region
 * height(int) height in pixel of the region
 */
THREE.TextureLoader.prototype.loadPixelArtRegion = function(path, pot, width, height) {
	var tex = this.load(path);
	tex.magFilter = THREE.NearestFilter; // Pixelated
	tex.matrixAutoUpdate = false; // Will manually set the UV transform matrix
	tex.matrix.elements[0] = width / pot;
	var uvv = height / pot;
	tex.matrix.elements[4] = uvv;
	tex.matrix.elements[7] = 1 - uvv;
	return tex;
};

/**
 * Load a sound effect from given path and return an Audio bound to the given AudioLister.
 *
 * path(string) path to a file
 * audioListener(AudioListener) a non null instance
 */
THREE.AudioLoader.prototype.loadSFX = function(path, audioListener) {
	var aud = new THREE.Audio(audioListener);
	aud.setVolume(0.5);
	this.load(path, function (ab){ aud.setBuffer(ab); });
	return aud;
};

/**
 * Returns a parametric function to create a tusk, to be used with ParametricGeometry or ParametricBufferGeometry.
 * An adaptation of the cone parametric functions https://www.wolframalpha.com/input/?i=cone+parametric+equations
 *
 * height(float) of the tusk in world units
 * thickness(float) of the tusk in world units
 */
function createTusk(height, thickness) {
	return function(u, v, vec3) {
		a = Math.pow(height+1, u) -1; // exp function from 0 to height
		b = Math.sqrt(u*height*height); // sqrt function from 0 to height

		u *= height;    // u from 0 to height
		v *= 2*Math.PI; // v from 0 to 2PI

		var x = thickness * (height - a) * Math.cos(v) / height;
		var y = b;
		var z = u + thickness * (height - a) * Math.sin(v) / height;

		vec3.set(x, y, z);
	};
}
