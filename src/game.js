//
// - Setup -
//

// Creates a ThreeJS renderer
var canvasElem = document.getElementById('glCanvas');
var renderer = new THREE.WebGLRenderer( {canvas: canvasElem} );
renderer.setSize(800, 600);
renderer.setClearColor(new THREE.Color(0xF0F0F0), 1.);

// FPS counter
var stats = new Stats();
stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom
document.body.appendChild(stats.dom);

// Load all the needed textures
var texLoader = new THREE.TextureLoader();
var cavemanTex = texLoader.loadPixelArt("data/caveman.png", true, 2, 2);
var mammothTex = texLoader.loadPixelArt("data/mammoth.png");
var indicatorTex = texLoader.loadPixelArt("data/indicator.png");
var foeArrowTex = texLoader.loadPixelArt("data/attacc.png");
var ggTex = texLoader.loadPixelArtRegion("data/gg.png", 128, 80, 60);
var wpTex = texLoader.loadPixelArtRegion("data/wp.png", 128, 80, 60);

// Creates a scene and a perspective camera
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(60, 8./6., 0.1, 100.);
scene.add(camera);

// Load all sounds and music
var audioListener = new THREE.AudioListener();
camera.add(audioListener);
var audioLoader = new THREE.AudioLoader();
// Music
var music = new THREE.Audio(audioListener);
music.setLoop(true);
music.setVolume(0.5);
audioLoader.load("data/A_Wintertale.mp3", function (ab){ music.setBuffer(ab); music.play(); });
// SFX
var kill = audioLoader.loadSFX("data/kill.wav", audioListener);
var step = audioLoader.loadSFX("data/step.wav", audioListener);

// End screens
var ggScene = new THREE.Scene();
var wpScene = new THREE.Scene();
var endCamera = new THREE.OrthographicCamera(-.5, .5, .5, -.5, -1, 1);
var gg = new THREE.Sprite(new THREE.SpriteMaterial( { map: ggTex } ));
var wp = new THREE.Sprite(new THREE.SpriteMaterial( { map: wpTex } ));
ggScene.add(gg);
wpScene.add(wp);

// Clock for timed things (animations, controls, ...)
var clock = new THREE.Clock();

// Controls
var controls = new THREE.FirstPersonControls(camera, canvasElem);
controls.movementSpeed = 10.;
controls.lookSpeed = 0.125;
controls.activeLook = false;
controls.lookSpeed = 1.;
controls.moveVertical = false;
controls.constrainDistanceFromOrigin = true;
controls.maxRadius = 100.;

// Debug stuff
scene.add(new THREE.PolarGridHelper(100, 16, 50, 16));
scene.add(new THREE.AxesHelper(100));

// Freezing Fog
scene.fog = new THREE.Fog(0xF0F0F0, .1, 25.); // Fixme: use FogExp2 instead?

// Lighting
// ambiant
var ambiLight = new THREE.AmbientLight(0xffffff, .4);
scene.add(ambiLight);
// directional
var dirLight = new THREE.DirectionalLight(0xffffff, .8);
dirLight.position.x = 1;
scene.add(dirLight);

// Ground
var grdGeo = new THREE.PlaneGeometry(250, 250); // FIXME replace with a disc
var grdMat = new THREE.MeshBasicMaterial( {color: 0xffff00 } );
grdGeo.rotateX(-Math.PI/2.);
var grd = new THREE.Mesh(grdGeo, grdMat);
scene.add(grd);

// Tusks
var tuskGeo = new THREE.ParametricBufferGeometry(createTusk(3, .3), 20, 10 );
var tuskMat = new THREE.MeshLambertMaterial( {color: 0xfffff0 } );
var tuskR = new THREE.Mesh(tuskGeo, tuskMat);
var tuskL = tuskR.clone();

camera.add(tuskR);
tuskR.rotateZ(  Math.PI / 12.);
tuskR.rotateX( -Math.PI / 4.);
tuskR.position.x =  2.3;
tuskR.position.y = -3.1;
tuskR.position.z = -3.0;

camera.add(tuskL);
tuskL.rotateZ( -Math.PI / 12.);
tuskL.rotateX( -Math.PI / 4.);
tuskL.position.x = -2.3;
tuskL.position.y = -3.1;
tuskL.position.z = -3.0;

// Caveman sprite (they attacc)
var spriteMaterial = new THREE.SpriteMaterial( { map: cavemanTex } );
//spriteMaterial.blending = THREE.CustomBlending; // FIXME Premul Alpha fucked-up by the Fog
//spriteMaterial.blendSrc = THREE.OneFactor;
var sprite = new THREE.Sprite(spriteMaterial);
sprite.scale.multiplyScalar(4.); // height = 4
sprite.translateY(2); // height/2
var foeArrow = new THREE.Sprite(new THREE.SpriteMaterial( { map: foeArrowTex } ));
foeArrow.scale.multiplyScalar(4.);
foeArrow.translateY(6);

// Mammoth (to protecc)
var mammoth = new THREE.Sprite(new THREE.SpriteMaterial( { map: mammothTex } ));
mammoth.scale.multiplyScalar(4.);
mammoth.translateY(2);
scene.add(mammoth);
var indicator = new THREE.Sprite(new THREE.SpriteMaterial( { map: indicatorTex } ));
indicator.scale.multiplyScalar(4.);
indicator.translateY(6);
scene.add(indicator);

// Instanciate foes
var cavemen = new Array(30);
for (var it=0; it<30; it++) {
	var inst = sprite.clone();
	var pos = new THREE.Vector2(THREE.Math.randFloat(20., 100.), 0.);
	pos.rotateAround(new THREE.Vector2(), THREE.Math.randFloat(0., 2*Math.PI));
	inst.position.x = pos.x;
	inst.position.z = pos.y;
	scene.add(inst);
	inst.isAlive = true;
	cavemen[it] = inst;

	inst = foeArrow.clone();
	inst.position.x = pos.x;
	inst.position.z = pos.y;
	scene.add(inst);
	cavemen[it].indicator = inst;
}

// End scenes

function ggLoop() {
	stats.begin();
	renderer.render(ggScene, endCamera);
	stats.end();
	requestAnimationFrame(ggLoop);

}

function wpLoop() {
	stats.begin();
	renderer.render(wpScene, endCamera);
	stats.end();
	requestAnimationFrame(wpLoop);
}

// Main loop
camera.position.x = 0;
camera.position.z = 7;
camera.position.y = 3;

camera.lookAt(0., 3, 0.);

// Is the baby mammoth ded yet?
var isDed = false;

function mainLoop() {
	stats.begin();

	// Timing from clock
	var elaps_t = clock.elapsedTime;
	var delta_t = clock.getDelta();
	var secTick = parseInt(elaps_t) & 0x01;
	var halfSecTick = parseInt(elaps_t * 2) & 0x01;

	// Blinks the indicator (Object3D.visible won't work)
	indicator.position.y = 6 + halfSecTick * 1000;

	// Animate caveman's sprite at 4 FPS
	var transval = parseInt(elaps_t*4.) % 4;
	cavemanTex.matrix.elements[6] = (transval == 0 || transval == 2)? 0: .5;
	cavemanTex.matrix.elements[7] = (transval == 0 || transval == 1)? 0: .5;

	// Move cavemen towards the center
	var allDed = true;
	for (var it=0; it<30; it++) {
		var inst = cavemen[it];
		if (inst.isAlive) {
			allDed = false;
			var to00 = inst.position.clone();
			to00.y = 3;
			if (to00.distanceTo(camera.position.clone().add(camera.getWorldDirection(new THREE.Vector3()).multiplyScalar(3.))) < 1.) {
				inst.isAlive = false;
				inst.visible = false;
				inst.indicator.visible = false;
				kill.play();
			}
			to00.y = 0.;
			if (to00.lengthSq() < 1.) { // Near the baby mammoth, game over
				isDed = true;
				break;
			}
			to00.normalize().negate();
			inst.translateOnAxis(to00, delta_t * .5);
			inst.indicator.translateOnAxis(to00, delta_t * .5);
			inst.indicator.position.y = 6 + halfSecTick * 1000; // Blink
		}
	}

	// play step sound
	if (controls.moveLeft || controls.moveRight || controls.moveForward || controls.moveBackward) {
		step.play();
	}

	controls.update(delta_t);
	renderer.render(scene, camera);

	stats.end();

	if (allDed) requestAnimationFrame(ggLoop); // GG!
	else if (isDed) requestAnimationFrame(wpLoop); // wp ...
	else requestAnimationFrame(mainLoop); // Loop on mainLoop
}
requestAnimationFrame(mainLoop);
